package ru.toppink.userservice.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.model.User;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto convert(User user) {
        UserDto head = Optional.ofNullable(user.getHead())
                .map(this::convert)
                .orElse(null);

        return UserDto.builder()
                .login(user.getLogin())
                .firstName(user.getFirstName())
                .secondName(user.getSecondName())
                .patronymic(user.getPatronymic())
                .role(user.getRole().getName())
                .head(head)
                .build();
    }
}
