package ru.toppink.userservice.mapper;

import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.model.User;

public interface UserMapper {

    UserDto convert(User user);
}
