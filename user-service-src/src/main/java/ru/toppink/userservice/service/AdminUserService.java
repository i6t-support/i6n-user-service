package ru.toppink.userservice.service;

import ru.toppink.userservice.model.User;

public interface AdminUserService {

    Object saveUser(String login, String password);

    User findByUserAndPassword(String login, String password);
}
