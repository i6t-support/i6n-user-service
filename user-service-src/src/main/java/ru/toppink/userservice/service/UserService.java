package ru.toppink.userservice.service;

import ru.toppink.common.securities.dao.Role;
import ru.toppink.userservice.api.dto.CreateApplicationDto;
import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.model.User;
import java.util.List;

public interface UserService {

    User findByLogin(String login);

    UserDto getUserInfo(String login);

    List<UserDto> findByRole(Role role);

    void createApplication(String login, CreateApplicationDto appealText);
}
