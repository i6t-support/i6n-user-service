package ru.toppink.userservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.userservice.api.exceptions.AuthorizeUserException;
import ru.toppink.userservice.api.exceptions.InternalException;
import ru.toppink.userservice.model.User;
import ru.toppink.userservice.model.UserRole;
import ru.toppink.userservice.repository.RoleRepository;
import ru.toppink.userservice.repository.UserRepository;
import ru.toppink.common.exception.SimpleException;
import ru.toppink.common.exception.UserLoginAlreadyBusyException;
import ru.toppink.common.exception.UserNotFoundException;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.common.utils.Optionals;

@Service
@Slf4j
public class AdminUserServiceImpl implements AdminUserService {

    private static final String DEFAULT_USER_ROLE = "ROLE_USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final SecurityService securityService;

    public AdminUserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, SecurityService securityService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.securityService = securityService;
    }

    @Override
    public User saveUser(String login, String password) {
        log.debug("saveUser - start: login = {}, password = {}", login, password);

        boolean loginIsAlreadyCreated = userRepository.findByLogin(login).isPresent();
        if (loginIsAlreadyCreated) {
            throw new UserLoginAlreadyBusyException(login);
        }

        User user = new User();
        user.setLogin(login);
        user.setPassword(securityService.encodePassword(password));
        user.setRole(getDefaultUserRole());

        return Optionals.of(
                userRepository.save(user),
                u -> log.debug("User saved: user = {}", u),
                () -> new SimpleException(login)
        );
    }

    @Override
    public User findByUserAndPassword(String login, String password) {
        log.debug("findByUserAndPassword - start: login = {}, password = {}", login, password);
        User user = userRepository.findByLogin(login)
                .orElseThrow(() -> new UserNotFoundException(login));

        String userPassword = user.getPassword();
        boolean notValidPassword = !securityService.matchPassword(password, userPassword);

        if (notValidPassword) {
            throw new AuthorizeUserException("Password is incorrect");
        }

        return Optionals.of(
                user,
                u -> log.debug("findByUserAndPassword - end: user = {}", u)
        );
    }

    UserRole getDefaultUserRole() {
        return roleRepository.findByName("ROLE_CURATOR").orElseThrow(
                () -> new InternalException("Default user role not found")
        );
    }
}
