package ru.toppink.userservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.common.kafka.KafkaSender;
import ru.toppink.common.securities.dao.Role;
import ru.toppink.userservice.api.dto.CreateApplicationDto;
import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.api.event.CreateApplicationEvent;
import ru.toppink.userservice.mapper.UserMapper;
import ru.toppink.userservice.model.User;
import ru.toppink.userservice.repository.UserRepository;
import ru.toppink.common.exception.UserNotFoundException;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.common.utils.Optionals;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final SecurityService securityService;
    private final KafkaSender kafkaSender;

    @Override
    public User findByLogin(String login) throws UserNotFoundException {
        return Optionals.ofOpt(
                userRepository.findByLogin(login),
                u -> log.debug("User found with login = {}", login),
                () -> new UserNotFoundException(login)
        );
    }

    @Override
    public UserDto getUserInfo(String login) {
        log.debug("Get user info initiated by {} for {}:", securityService.getLogin(), login);
        User user = findByLogin(login);
        return Optionals.of(
                userMapper.convert(user),
                u -> log.debug("Get user info for user = {}: {}", login, u)
        );
    }

    @Override
    public List<UserDto> findByRole(Role role) {
        log.debug("Get user by role: role = {}", role);
        return Optionals.of(
                userRepository.findAll().stream()
                        .filter(user -> user.getRole().getName().equals(role.getRaw()))
                        .map(userMapper::convert)
                        .collect(Collectors.toList()),
                result -> log.debug("Get user by role finished: {}", result)
        );
    }

    @Override
    public void createApplication(String login, CreateApplicationDto createApplicationDto) {
        log.debug("Start create application by login = {} with message = {}", login, createApplicationDto);

        User user = findByLogin(login);

        CreatedApplicationDto createdApplicationDto = CreatedApplicationDto.builder()
                .loginClient(login)
                .firstNameClient(user.getFirstName())
                .lastNameClient(user.getSecondName())
                .patronymicClient(user.getPatronymic())
                .phoneNumber("+" + (100_000_00_00 + ThreadLocalRandom.current().nextLong(900_000_00_00L)))
                .contractNumber("" + (100_000_00_00 + ThreadLocalRandom.current().nextLong(900_000_00_00L)))
                .address("г. Статический, ул. Постоянная, д. 49 корпус 2, кв. 205")
                .appealText(createApplicationDto.getAppealText())
                .reason(createApplicationDto.getReason())
                .build();

        CreateApplicationEvent createApplicationEvent = new CreateApplicationEvent(createdApplicationDto);

        kafkaSender.sendToUserOut(createApplicationEvent);
    }
}
