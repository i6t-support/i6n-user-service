package ru.toppink.userservice.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.toppink.userservice.service.UserService;
import ru.toppink.common.securities.UserFactory;

@Configuration
@RequiredArgsConstructor
public class CommonConfig {

    private final UserService userService;

    @Bean
    @Primary
    public UserFactory userFactory() {
        return userService::findByLogin;
    }
}
