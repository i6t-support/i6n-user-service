package ru.toppink.userservice.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.common.kafka.KafkaSender;
import ru.toppink.common.securities.JwtProvider;
import ru.toppink.common.securities.dto.RegistrationRequest;
import ru.toppink.userservice.api.event.UserAuthorizeEvent;
import ru.toppink.userservice.model.User;
import ru.toppink.userservice.service.AdminUserService;
import java.time.OffsetDateTime;
import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/security")
public class AuthController {
    private final JwtProvider jwtProvider;
    private final AdminUserService adminUserService;
    private final KafkaSender kafkaSender;

    @PostMapping("/register")
    public void registerUser(@RequestBody @Valid RegistrationRequest registrationRequest) {
        adminUserService.saveUser(registrationRequest.getLogin(), registrationRequest.getPassword());
    }

    @PostMapping("/auth")
    public String auth(@RequestBody RegistrationRequest request) {
        User user = adminUserService.findByUserAndPassword(request.getLogin(), request.getPassword());
        String token = jwtProvider.generateToken(user);
        kafkaSender.sendToUserOut(new UserAuthorizeEvent(user.getLogin(), OffsetDateTime.now()));
        return token;
    }
}