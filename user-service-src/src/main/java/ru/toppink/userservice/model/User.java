package ru.toppink.userservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user_table")
@NoArgsConstructor
@AllArgsConstructor
public class User extends ru.toppink.common.securities.dao.User {

    @Id
    @GeneratedValue
    private UUID id;

    private String login;

    private String password;

    private String firstName;

    private String secondName;

    private String patronymic;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "head_id")
    private User head;

    @ManyToOne
    private UserRole role;
}
