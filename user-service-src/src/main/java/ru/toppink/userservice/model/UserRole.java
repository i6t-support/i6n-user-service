package ru.toppink.userservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "role_table")
@NoArgsConstructor
@AllArgsConstructor
public class UserRole extends ru.toppink.common.securities.dao.UserRole {

    @Id
    private UUID id;

    private String name;
}
