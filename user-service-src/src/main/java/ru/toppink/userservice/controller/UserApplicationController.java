package ru.toppink.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.userservice.api.dto.CreateApplicationDto;
import ru.toppink.userservice.api.swagger.UserApplicationSwaggerApi;
import ru.toppink.userservice.service.UserService;

@RestController
@RequiredArgsConstructor
public class UserApplicationController implements UserApplicationSwaggerApi {

    private final UserService userService;
    private final SecurityService securityService;

    @Override
    @PreAuthorize("isAuthenticated() and hasRole('USER')")
    public void createApplication(CreateApplicationDto createApplicationDto) {
        userService.createApplication(securityService.getLogin(), createApplicationDto);
    }
}
