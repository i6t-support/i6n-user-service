package ru.toppink.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.common.securities.dao.Role;
import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.api.swagger.UserSwaggerApi;
import ru.toppink.userservice.service.UserService;
import ru.toppink.common.securities.SecurityService;
import java.util.List;

@RestController
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class UserController implements UserSwaggerApi {

    private final UserService userService;
    private final SecurityService securityService;

    @Override
    public UserDto getCurrentUserProfile() {
        return userService.getUserInfo(securityService.getLogin());
    }

    @Override
    public UserDto getFullUserProfile(String login) {
        return userService.getUserInfo(login);
    }

    @Override
    public List<UserDto> findByRole(Role role) {
        return userService.findByRole(role);
    }
}
