INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('fd2dd230-2bd9-4b28-bebd-5f369680a67b', 'solovyev',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Полина', 'Соловьева', 'Егоровна',
		'bb14e85d-dc43-45b5-83e7-80c371aab277', 'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('b061f9da-1389-4e87-9e5e-c128c6cdab9b', 'mironova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ева', 'Миронова', 'Леонидовна',
		'ec40ed5d-4aae-4cf8-b1be-3252ef1e64b9', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('06c287cd-a410-4f1d-9bc5-895d1c13f702', 'kirillovdv',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Дмитрий', 'Кириллов', 'Васильевич',
		'1a45bd1f-9e53-4a22-b1d7-58242be3398d', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('92bff8d5-3541-4f2c-baa2-39b04456608d', 'kirillovae',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Александр', 'Кириллов', 'Елисеевич',
		'10c92384-ec6c-4cc8-8990-2f5cdf5d53ae', '6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('e4e60c40-6850-42d0-a4b7-1c42db0a7e85', 'molchanov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Захар', 'Молчанов', 'Дмитриевич',
		'10c92384-ec6c-4cc8-8990-2f5cdf5d53ae', '6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('339464d8-fb71-435f-b654-8e3017c74adc', 'jykov', '$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK',
		'Лев', 'Жуков', 'Михайлович',
		'05a32293-5b05-4b42-879e-34dc37562f87', 'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('68292c14-5269-454d-a848-3c62ae36a3ca', 'maksimova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Мария', 'Максимова', 'Никитична',
		'0eb44a57-4683-43d1-9fb4-7fb25e267ec5', 'db5a445a-d89c-4940-aeff-b1ce74aacf9e');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('618f34bc-6c7c-4c44-a93d-a3f77f38eb40', 'bylgakov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Анна', 'Булгакова', 'Артёмовна',
		'844c9833-e20a-4483-8ea7-d3017e8c59db', '6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('e02526c1-a2b0-4170-b74d-8aa07b7f047b', 'kondrashov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Егор', 'Кондрашов', 'Даниилович',
		'66ab2c4e-7c93-4f72-b5e4-fa8065570f09', 'f2813055-af39-4565-b81c-18ba05c14220');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('09d9fdf6-c9ce-4edc-a014-363d5c9cce81', 'stolyarova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Алиса', 'Столярова', 'Германовна',
		'42bfd8ef-687b-480e-886b-326ca001455d', 'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('9e88ae5c-8eb6-4989-b8a1-1dc9c9f6d524', 'sidorov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Даниил', 'Сидоров', 'Александрович',
		'10c92384-ec6c-4cc8-8990-2f5cdf5d53ae', '6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('4fe047a2-15e4-4921-9a86-02c661e159b0', 'karaseva',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Арина', 'Карасева', 'Владиславовна',
		'ce137193-dd8f-4104-90d7-9f0d01f4b35a', 'db5a445a-d89c-4940-aeff-b1ce74aacf9e');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('f2727db2-3306-4035-92d0-c9f28d7a2978', 'smirnov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Марк', 'Смирнов', 'Игоревич',
		'844c9833-e20a-4483-8ea7-d3017e8c59db', '6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('323fac4d-457b-441e-9ee5-f3dfbb41368c', 'frolov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Фёдор', 'Фролов', 'Степанович',
		'e47335d9-6187-486d-9da6-079399874302', 'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('fae77339-1b7a-47c9-9b87-d0b77b528ca2', 'sokolov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Мария', 'Соколова', 'Ильинична',
		'05a32293-5b05-4b42-879e-34dc37562f87', 'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('670e75dc-8c33-47e9-98d9-497f5b13bba4', 'sorokina',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Анастасия', 'Сорокина', 'Марковна',
		'ec40ed5d-4aae-4cf8-b1be-3252ef1e64b9', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('7eeb8cc8-4515-4a0b-aec2-7ec735db82ea', 'cvetkov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Леонид', 'Цветков', 'Степанович',
		'bb14e85d-dc43-45b5-83e7-80c371aab277', 'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('2d8407c5-fa65-473f-bae2-f8f3baec3279', 'sorokina2',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Сафия', 'Сорокина', 'Дмитриевна',
		'42bfd8ef-687b-480e-886b-326ca001455d', 'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('5166bb67-1761-4a2f-b648-a4342f752c57', 'karpov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Илья', 'Карпов', 'Денисович',
		'2c23ba1b-092e-4584-843f-e5031599dc3e', 'd79dac2f-8bcd-4e66-81d5-69f612bf6f1b');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('d9fe6a2d-37f5-4db1-af1f-5b54992fc60e', 'ivanova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ева', 'Иванова', 'Артуровна',
		'cfb9db21-e866-40da-9e95-a29805820b7d', 'a548a24e-a524-4dd4-b2b7-a0c5ae9709db');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('a1b29fed-bd9d-46dc-921f-06a011ae0b6f', 'merkylova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Алина', 'Меркулова', 'Степановна',
		'cfb9db21-e866-40da-9e95-a29805820b7d', 'a548a24e-a524-4dd4-b2b7-a0c5ae9709db');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('0fb1bacb-278a-4ccb-aeff-8e1851fcc85d', 'kypriyanov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Давид', 'Куприянов', 'Николаевич',
		'7394ae9a-91ee-4b44-a17a-d41379f446dd', 'f2813055-af39-4565-b81c-18ba05c14220');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('49042d5f-665d-4ab6-8f04-a4ac74d3b3d2', 'shishkina',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Сафия', 'Шишкина', 'Михайловна',
		'1a45bd1f-9e53-4a22-b1d7-58242be3398d', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('c5dc608d-8e4b-49dc-97fe-8260f55eedf8', 'kyzmin',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Максим', 'Кузьмин', 'Артёмович',
		'ce137193-dd8f-4104-90d7-9f0d01f4b35a', 'db5a445a-d89c-4940-aeff-b1ce74aacf9e');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('40e76aa5-8b60-4d9c-850e-fbaf915896ef', 'voloshina',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ксения', 'Волошина', 'Романовна',
		'cfb9db21-e866-40da-9e95-a29805820b7d', 'a548a24e-a524-4dd4-b2b7-a0c5ae9709db');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('f7df74a7-836e-4ba2-a1bf-cc9c275ac198', 'petrov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Артём', 'Петров', 'Никитич',
		'53890f4b-36c5-4867-9113-a75f012fbc0d', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('9f6704ed-293f-4726-b24e-4f0f87bd68c6', 'nazarova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Алиса', 'Назарова', 'Андреевна',
		'e47335d9-6187-486d-9da6-079399874302', 'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('ca95fcf4-6fcf-47b1-a3a7-076522db9041', 'ivanov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Даниил', 'Иванов', 'Иванович',
		'53890f4b-36c5-4867-9113-a75f012fbc0d', '42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('473aee44-1724-4cdd-9084-dd301e02e89d', 'volkov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Марк', 'Волков', 'Никитич',
		'2c23ba1b-092e-4584-843f-e5031599dc3e', 'd79dac2f-8bcd-4e66-81d5-69f612bf6f1b');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('18549708-ac84-4f45-8994-a78d761c28e7', 'kostin',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Иван', 'Костин', 'Романович',
		'2c23ba1b-092e-4584-843f-e5031599dc3e', 'd79dac2f-8bcd-4e66-81d5-69f612bf6f1b');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('05a32293-5b05-4b42-879e-34dc37562f87', 'demina',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Екатерина', 'Демина', 'Данииловна', NULL,
		'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('e47335d9-6187-486d-9da6-079399874302', 'morozov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Роберт', 'Морозов', 'Богданович', NULL,
		'a014a335-9c63-4a10-ac42-8ad0477169fa');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('ec40ed5d-4aae-4cf8-b1be-3252ef1e64b9', 'aksenova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Александра', 'Аксенова', 'Романовна', NULL,
		'42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('1a45bd1f-9e53-4a22-b1d7-58242be3398d', 'lukyanova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Антонина', 'Лукьянова', 'Тимофеевна', NULL,
		'42db5043-61cc-4d03-b191-9e59cb8f5822');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('bb14e85d-dc43-45b5-83e7-80c371aab277', 'lobanova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ольга', 'Лобанова', 'Фёдоровна', NULL,
		'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('42bfd8ef-687b-480e-886b-326ca001455d', 'blinof',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Юрий', 'Блинов', 'Русланович', NULL,
		'f0348f2a-17f1-4fc7-8e7b-15e05f388a5a');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('844c9833-e20a-4483-8ea7-d3017e8c59db', 'mayoirov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Екатерина', 'Майорова', 'Романовна', NULL,
		'6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('10c92384-ec6c-4cc8-8990-2f5cdf5d53ae', 'morozova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ева', 'Морозова', 'Михайловна', NULL,
		'6a07aec7-1920-43f9-9b43-4b4ddf248261');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('0eb44a57-4683-43d1-9fb4-7fb25e267ec5', 'zvecov',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Арсений', 'Швецов', 'Тимофеевич', NULL,
		'db5a445a-d89c-4940-aeff-b1ce74aacf9e');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('ce137193-dd8f-4104-90d7-9f0d01f4b35a', 'novikova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Ульяна', 'Новикова', 'Григорьевна', NULL,
		'db5a445a-d89c-4940-aeff-b1ce74aacf9e');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('66ab2c4e-7c93-4f72-b5e4-fa8065570f09', 'platonova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Арина', 'Платонова', 'Фёдоровна', NULL,
		'f2813055-af39-4565-b81c-18ba05c14220');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('7394ae9a-91ee-4b44-a17a-d41379f446dd', 'alekseeva',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Анастасия', 'Алексеева', 'Александровна',
		NULL, 'f2813055-af39-4565-b81c-18ba05c14220');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('cfb9db21-e866-40da-9e95-a29805820b7d', 'savin', '$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK',
		'Константин', 'Савин', 'Артёмович', NULL,
		'a548a24e-a524-4dd4-b2b7-a0c5ae9709db');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('2c23ba1b-092e-4584-843f-e5031599dc3e', 'sidorovmg',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Михаил', 'Сидоров', 'Георгиевич', NULL,
		'd79dac2f-8bcd-4e66-81d5-69f612bf6f1b');
INSERT INTO public.user_table (id, login, password, first_name, second_name, patronymic, head_id, role_id)
VALUES ('53890f4b-36c5-4867-9113-a75f012fbc0d', 'maslova',
		'$2a$10$FjoEdrADJ6hK/eF1j0uHl.PTJVubp3DTjVU09AzFAVB9RN2NztMpK', 'Алиса', 'Маслова', 'Александровна', NULL,
		'42db5043-61cc-4d03-b191-9e59cb8f5822');
