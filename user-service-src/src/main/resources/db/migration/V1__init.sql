CREATE TABLE role_table
(
	id       UUID        NOT NULL
		CONSTRAINT role_table_pk PRIMARY KEY,
	name     VARCHAR(64) NOT NULL,
	_default BOOLEAN     NOT NULL DEFAULT FALSE
);


CREATE TABLE user_table
(
	id          UUID NOT NULL
		CONSTRAINT user_table_pk PRIMARY KEY,
	login       VARCHAR(50),
	password    VARCHAR(500),
	first_name  VARCHAR(32),
	second_name VARCHAR(32),
	patronymic  VARCHAR(32),
	head_id 	UUID,
	role_id     UUID
		CONSTRAINT user_table_role_table_id_fk REFERENCES role_table
);

CREATE UNIQUE INDEX user_table_login_uindex ON user_table (login);
