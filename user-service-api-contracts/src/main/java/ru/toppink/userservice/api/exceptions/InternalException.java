package ru.toppink.userservice.api.exceptions;

import lombok.extern.slf4j.Slf4j;
import ru.toppink.common.exception.SimpleException;
import java.time.Clock;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.util.UUID;

@Slf4j
public class InternalException extends SimpleException {

    private static final OffsetDateTime startup = OffsetDateTime.now(Clock.systemUTC());
    private static final Long id = 1L;

    public InternalException(String message) {
        super("INTERNAL_MESSAGE#" + startup.toString() + ":" + id);
        log.error(message);
    }
}
