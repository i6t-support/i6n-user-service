package ru.toppink.userservice.api.feign;

import org.springframework.cloud.openfeign.FeignClient;
import ru.toppink.userservice.api.swagger.UserSwaggerApi;

@FeignClient(
        name = "user-client",
        url = "${feign.services.users.url:https://user.toppink.ru}",
        primary = false
)
public interface UserClient extends UserSwaggerApi {

}
