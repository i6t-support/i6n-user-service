package ru.toppink.userservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.applicationservice.api.enums.ApplicationReason;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateApplicationDto {

    @NotNull
    private ApplicationReason reason;

    @NotBlank
    private String appealText;
}
