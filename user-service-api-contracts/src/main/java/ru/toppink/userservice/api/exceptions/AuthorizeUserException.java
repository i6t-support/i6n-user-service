package ru.toppink.userservice.api.exceptions;

import ru.toppink.common.exception.SimpleException;

public class AuthorizeUserException extends SimpleException {

    public AuthorizeUserException(String message) {
        super(message);
    }
}
