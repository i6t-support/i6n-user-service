package ru.toppink.userservice.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateApplicationEvent {

    private CreatedApplicationDto createdApplicationDto;
}
