package ru.toppink.userservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private String login;
    private String firstName;
    private String secondName;
    private String patronymic;
    private String role;
    private UserDto head;
}
