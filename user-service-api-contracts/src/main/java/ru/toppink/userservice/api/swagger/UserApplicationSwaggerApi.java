package ru.toppink.userservice.api.swagger;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.toppink.userservice.api.dto.CreateApplicationDto;

@RequestMapping("/api/v1/user/app")
public interface UserApplicationSwaggerApi {

    @PostMapping
    @ApiOperation("Создать заявку")
    void createApplication(@RequestBody CreateApplicationDto createDto);
}
