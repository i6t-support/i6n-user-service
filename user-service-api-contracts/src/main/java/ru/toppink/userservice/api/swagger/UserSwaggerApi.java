package ru.toppink.userservice.api.swagger;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.toppink.common.securities.dao.Role;
import ru.toppink.userservice.api.dto.UserDto;
import java.util.List;

@RequestMapping("/api/v1/user")
public interface UserSwaggerApi {

    @GetMapping("/current")
    @ApiOperation("Получить информацию о текущем пользователе")
    UserDto getCurrentUserProfile();

    @GetMapping("/full")
    @ApiOperation("Получить полную информацию о пользователе")
    UserDto getFullUserProfile(@RequestParam String login);

    @GetMapping("/find")
    @ApiOperation("Получить список пользователей с определенной ролью")
    List<UserDto> findByRole(@RequestParam Role role);
}
