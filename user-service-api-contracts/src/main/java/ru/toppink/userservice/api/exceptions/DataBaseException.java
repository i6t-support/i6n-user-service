package ru.toppink.userservice.api.exceptions;

import ru.toppink.common.exception.SimpleException;

public class DataBaseException extends SimpleException {

    public DataBaseException(String message) {
        super(message);
    }
}
